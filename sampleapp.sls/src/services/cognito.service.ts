import {Injector} from '@sailplane/injector'
import {Logger} from "@sailplane/logger";
import {CognitoRepo} from '../repositories/cognito.repository'
export class CognitoService {
    private logger = new Logger('Cognito Service');

    constructor(private repository: CognitoRepo){}

    public async cognitoRegister(user){
        await this.repository.createAdminUser(user);
        this.logger.info('after create user')
        await this.repository.adminSetPassword(user)
        this.logger.info('after set user password')
        return "user created successfully"
    }

    public async cognitoLogin(user){
        return await this.repository.adminInitiateAuth(user)
    }

}
Injector.register(CognitoService,[CognitoRepo])