import { CountryRepo } from '../repositories/country.repository'
import {Logger, LogLevels} from "@sailplane/logger";
import {Injector} from '@sailplane/injector'
import { uuid } from 'uuidv4';
import { Country } from "../models/country";

export class CountryService {
    private logger = new Logger('Countries Service');
    
    constructor(private repository: CountryRepo){}

    public async createCountry(data){
        this.logger.info(`creating country ${data.name} with body ${data}`)
        let item: Country = {
            countryId: uuid(),
            name: data.name,
            population:  data.population,
            numOfStates: data.numOfStates
        }
        await this.repository?.createCountry(item)
        this.logger.info(`country created successfully`)
    }

    public async publishCountry(data){
        this.logger.info(`publishing country ${data.name} with body ${data}`)
        let item: Country = {
            countryId: uuid(),
            name: data.name,
            population:  data.population,
            numOfStates: data.numOfStates
        }
        await this.repository?.publishCountry(item)
        this.logger.info(`country published successfully`)
    }

    public async getCountries(){
        this.logger.info(`getting all countries`)
        let countries = await this.repository?.getCountries();
        this.logger.info(`Countries after return ${JSON.stringify(countries)}`)
        return {
            items:countries,
            Count: countries!.length
        }
    }

    public async getCountryByName(name){
        this.logger.info(`getting country ${name}`)
        let country = await this.repository?.getCountryByName(name);
        if(country.Count == 0) return null
        this.logger.info(`country returned ${country}`)
        return country
    }

    public async updateCountry(data,name){
        this.logger.info(`updainf country ${name} with body ${data}`)
        let country = await this.repository?.getCountryByName(name)
        if(country?.Count == 0) throw new Error('country not found!')
        await this.repository?.updateCountry(data,name) 
        this.logger.info(`country has been updated`)
        return 'country has been updated'
    }
}
Injector.register(CountryService,[CountryRepo])