/**
 * Fields in a request to create a single Country item.
 */
 export interface CreateCountryRequest {
    name: string
    population: number
    numOfStates: number
  }
  