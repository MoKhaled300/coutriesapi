/**
 * Fields in a request to update a single Country item.
 */
 export interface UpdateCountryRequest {
    population: number
    numOfStates: number
  }
  