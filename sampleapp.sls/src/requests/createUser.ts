/**
 * Fields in a request to create a single user item.
 */
 export interface CreateUserRequest {
    email: string
    password: string
  }
  