/**
 * Fields in a request to login user.
 */
 export interface LoginRequest {
    email: string
    password: string
  }
  