import * as faker from "faker";

export function generateCountryData() {
  return {
    countryId: faker.datatype.uuid(),
    name: faker.name.title(),
    numOfStates: faker.datatype.number(),
    population: faker.datatype.number()
  };
}

export function generateCountriesData(n: number = 1, overide = {}) {
  return Array.from(
    {
      length: n,
    },
    (_, i) => {
      return generateCountryData();
    }
  );
}
