import {CognitoService} from '../services/cognito.service';
// import {Logger} from '@sailplane/logger'

// jest.mock("logger", () => {
//     return {
//         info: jest.fn()
//     }
//   });


  describe('cognito Service',()=>{
    let cognitoService: CognitoService;
    let cognitoRepo: any;

    let user = {};
    beforeEach(() => {
        cognitoRepo = {
            createAdminUser: jest.fn(),
            adminSetPassword: jest.fn(),
            adminInitiateAuth: jest.fn()
    }
    cognitoService = new CognitoService(cognitoRepo);
    });
  
      describe('create User function',()=>{
          test('it should create country', async ()=>{
              let x = await cognitoService.cognitoRegister(user)  
              expect(x).toBe("user created successfully")
          })
      })
      describe('login user',()=>{
        test('it should login',async()=>{
          let userLogin = await cognitoService.cognitoLogin(user)
          expect(userLogin).toBe(undefined)
        })
      })
  })