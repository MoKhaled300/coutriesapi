import {CountryService} from '../services/country.service';
import {generateCountriesData , generateCountryData} from './utils/generate';

jest.mock("uuid", () => {
  return {
      v4: jest.fn(() =>"123")
  }
});
jest.spyOn(console, 'info').mockImplementation(() => {});
jest.spyOn(console, 'debug').mockImplementation(() => {});


describe('Country Service',()=>{
  let countriesService: CountryService;
  let countriesRepo: any;
  let countries = generateCountriesData(5);
  let country = generateCountryData()
  beforeEach(() => {
    countriesRepo = {
      createCountry: jest.fn(),
      getCountries: jest.fn(()=>countries),
      getCountryByName: jest.fn().mockReturnValueOnce(country).mockReturnValueOnce({Count:0}),
      updateCountry: jest.fn()
  }
    countriesService = new CountryService(countriesRepo);
  });

    describe('create country function',()=>{
        test('it should create country', async ()=>{
            let x = await countriesService.createCountry(country)  
            country.countryId = "123"    
            expect(countriesRepo.createCountry.mock.calls[0][0]).toStrictEqual(country)
            expect(x).toBe(undefined)
        })
    })
    describe('get all contries',()=>{
      test('it should return array of countries',async()=>{
        let countriesReturned = await countriesService.getCountries()
        let result = {
          Count: countries.length,
          items: countries
        }
        expect(countriesReturned).toStrictEqual(result)
      })
    })
    describe('get Country by name',()=>{
      test('it should return a country',async ()=>{
        let countryFound = await countriesService.getCountryByName('egypt')
        expect(countryFound).toStrictEqual(country)
      })
      test('get null when find non exist country',async ()=>{
        expect(countriesRepo.getCountryByName()).toStrictEqual(country)
        let countryFound = await countriesService.getCountryByName('egypt')
        expect(countryFound).toStrictEqual(null)
      })
    })
    describe('update Country by name',()=>{
      test('it should update country',async ()=>{
        let updated = await countriesService.updateCountry(country,'egypt')
        expect(updated).toStrictEqual('country has been updated')
      })
      test('it should not find country',async ()=>{
        // call countries repo first time to return country and be ready for second invoke by test
        expect(countriesRepo.getCountryByName()).toStrictEqual(country)
        await expect(countriesService.updateCountry(country,'egypt')).rejects.toThrow()
      })
    })
})