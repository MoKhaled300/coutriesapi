export interface Country {
    countryId: string
    name: string
    numOfStates: number
    population: number
  }
  