import * as jwt from 'jsonwebtoken'

export async function verify(token: string) {
    const secret = process.env.JWT_SECRET
    const decoded: any = jwt.verify(token,secret!);
    return decoded;
}

export async function signToken(email: string) {
  const secret = process.env.JWT_SECRET
  return jwt.sign({email}, secret!, {
    expiresIn: process.env.JWT_EXP // expires in 24 hours
  });
}