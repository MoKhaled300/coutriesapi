import * as bcrypt from 'bcryptjs'

export async function hashPass(password: string){
    const salt = await bcrypt.genSalt(10);
    let hash = await bcrypt.hash(password, salt);
    return hash
}
