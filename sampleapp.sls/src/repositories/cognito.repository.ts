import {Logger} from "@sailplane/logger";
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import {Injector} from '@sailplane/injector'

export class CognitoRepo {
    private readonly userPoolId = process.env.USER_POOL_ID;
    private readonly clientId =  process.env.CLIENT_ID;
    private readonly cognito = new CognitoIdentityServiceProvider();
    private logger = new Logger('Cognito Repository');

    constructor(){}

    public async createAdminUser(user){
        this.logger.info(`cognito creates user with email = ${user.email}`)
        return await this.cognito.adminCreateUser({
            UserPoolId: this.userPoolId!,
            Username: user.email,
            MessageAction: 'SUPPRESS',
            TemporaryPassword: user.password
          }).promise()
    }

    public async adminSetPassword (user){
        this.logger.info(`cognito sets user with password = ${JSON.stringify(user)}`)
        await this.cognito.adminSetUserPassword({
            Password: user.password,
            UserPoolId: this.userPoolId!,
            Username: user.email, /* required */
            Permanent: true
          }).promise()
    }

    public async adminInitiateAuth(user){
        this.logger.info(`cognito initiates login request = ${JSON.stringify(user)}`)
        return await this.cognito.adminInitiateAuth({
            ClientId: this.clientId!,    
            UserPoolId: this.userPoolId!,
            AuthFlow: "ADMIN_USER_PASSWORD_AUTH",
            AuthParameters: {
              USERNAME: user.email,
              PASSWORD: user.password
            }
          }).promise()
    }

    public async adminDelteUser(email){
      await this.cognito.adminDeleteUser({
        UserPoolId: this.userPoolId!,
        Username: email
      })
    }
}
Injector.register(CognitoRepo)