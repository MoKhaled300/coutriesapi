import { Country } from "../models/country";

import {Injector} from '@sailplane/injector'
import * as AWS from 'aws-sdk'
import * as AWSXRay from 'aws-xray-sdk'
import {Logger, LogLevels} from "@sailplane/logger";

export class CountryRepo {

    private readonly XAWS = AWSXRay.captureAWS(AWS)
    private readonly docClient: AWS.DynamoDB.DocumentClient = new this.XAWS.DynamoDB.DocumentClient()
    private readonly countriesTable = process.env.COUNTRY_TABLE
    private logger = new Logger('Countries Repository');

    constructor() {}

    async publishCountry(request){
       //publish on createCountry tobic
       let iotdata = new AWS.IotData({ endpoint: 'a2mq594uxa3le7-ats.iot.us-east-1.amazonaws.com' });
       let params = {
        topic: "CountryTopic",
        payload: JSON.stringify(request),
        qos: 1
    };

    let req = iotdata.publish(params)
    req
    .on('success', () => this.logger.info("Success"))
    .on('error', () => this.logger.info("Error"))
    .send()
    return 'done'
    }

    async createCountry(request){
       
        return await this.docClient.put({
              TableName: this.countriesTable!,
              Item: request
          }).promise()
  
      }
  
    async getCountries(): Promise<Country[]> {

        let params: any= {
            TableName: this.countriesTable!
        }
        let scanResults = [] as any;
        let items;
        do{
            items =  await this.docClient.scan(params).promise();
            items.Items.forEach((item) => scanResults.push(item));
            params.ExclusiveStartKey  = items.LastEvaluatedKey;
        }while(typeof items.LastEvaluatedKey != "undefined");
        
        return scanResults as Country[];
    }

    public async updateCountry(updatedCountry, name: string){
        await this.docClient.update({
            TableName: this.countriesTable!,
            Key: {
                'name': name
            },
            UpdateExpression: 'set population = :p, numOfStates = :n',
            ExpressionAttributeValues: {
                ':p': updatedCountry.population,
                ':n': updatedCountry.numOfStates
            },
            ReturnValues:"UPDATED_NEW"
        }).promise()
    }
   
    public async getCountryByName(name: string): Promise<AWS.DynamoDB.QueryOutput>{
     return await this.docClient.query({
            TableName: this.countriesTable!,
            KeyConditionExpression: '#namefield = :n',
            ExpressionAttributeValues: {
                ':n': name
            },
            ExpressionAttributeNames: {
                "#namefield": "name"
            }
        }).promise()
    
    }
}
Injector.register(CountryRepo)