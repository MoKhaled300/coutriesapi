import 'source-map-support/register'

import * as LambdaUtils from "@sailplane/lambda-utils";
import * as createError from "http-errors";


import { UpdateCountryRequest } from '../requests/updateCountry'
import {CountryService} from '../services/country.service'
import {Logger, LogLevels} from "@sailplane/logger";
import { Injector } from '@sailplane/injector';

let logger = new Logger('update Countries');

export const handler = LambdaUtils.wrapApiHandler(async (event: LambdaUtils.APIGatewayProxyEvent) => {
  try {
    const name = event.pathParameters!.countryName
    const updatedCountry: UpdateCountryRequest = event.body!
    
    await Injector.get(CountryService)?.updateCountry(updatedCountry,name);
  
    logger.info(`updating Country ${name} to be ${updatedCountry}`)
    return 'The Country has been updated.'
    
  } catch (error) {
    logger.error(`error has occured ${error}`)
    throw new createError.NotFound(error.message)
  }

})

