import 'source-map-support/register'

import * as LambdaUtils from "@sailplane/lambda-utils";
import * as createError from "http-errors";

import { CreateUserRequest } from '../requests/createUser'
import {Logger} from "@sailplane/logger";
import { Injector } from '@sailplane/injector';
import { CognitoService } from '../services/cognito.service';

let logger = new Logger('Create cognito user');

export const handler = LambdaUtils.wrapApiHandler(async (event: LambdaUtils.APIGatewayProxyEvent) => { 
    try{
      const user: CreateUserRequest =event.body!
    
      logger.info(`Create User ${user.email}`)

      let result = await Injector.get(CognitoService)?.cognitoRegister(user)  
      return {
          statusCode: 201,
          body: result
      }

    }catch(error){
      logger.error(`error has occured ${error}`)
      throw new createError.BadRequest(error.message)
    }
    
  })  
  