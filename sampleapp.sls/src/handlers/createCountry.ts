import 'source-map-support/register'

import {CountryService} from '../services/country.service'

import * as LambdaUtils from "@sailplane/lambda-utils";
import * as createError from "http-errors";

import { CreateCountryRequest } from '../requests/createCountry'
import {Logger, LogLevels} from "@sailplane/logger";
import {Injector} from '@sailplane/injector'

let logger = new Logger('Create Countries');

export const handler = LambdaUtils.wrapApiHandler(async (event: LambdaUtils.APIGatewayProxyEvent) => { 
  try{
    const newCountry: CreateCountryRequest =event.body!
  
    logger.info(`Create Country ${newCountry.name} with data ${newCountry}`)
  
    await Injector.get(CountryService)?.publishCountry(newCountry)
  
    logger.info(`returned after await`)
  
    return 'country has been created'
  }catch(error){
    logger.error(`error has occured ${error}`)
    throw new createError.InternalServerError(error.message)
  }
  
})  
