import 'source-map-support/register'

import * as LambdaUtils from "@sailplane/lambda-utils";
import * as createError from "http-errors";


import {CountryService} from '../services/country.service'
import {Logger, LogLevels} from "@sailplane/logger";
import { Injector } from '@sailplane/injector';


let logger = new Logger('Get Countries');


export const handler = LambdaUtils.wrapApiHandler(async (event: LambdaUtils.APIGatewayProxyEvent) => {
  try {
    const countries = await Injector.get(CountryService)?.getCountries()

  logger.info(`Get Countries ${JSON.stringify(countries)}`)

  if(!countries) return {
    statusCode: 404,
    body: "Countries not Exist!"
  }  

  return countries
  } catch (error) {
    logger.error(`error has occured ${error}`)
    throw new createError.NotFound(error.message)
  }
  
})

