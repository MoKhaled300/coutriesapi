import 'source-map-support/register'

import * as LambdaUtils from "@sailplane/lambda-utils";
import * as createError from "http-errors";

import {Logger, LogLevels} from "@sailplane/logger";

import {CountryService} from '../services/country.service'
import { Injector } from '@sailplane/injector';

let logger = new Logger('Get one country by name');


export const handler = LambdaUtils.wrapApiHandler(async (event: LambdaUtils.APIGatewayProxyEvent) => {
  try {
    logger.info(`Get country ${event.pathParameters!.countryName}`)

    let name: any = event.pathParameters!.countryName
    const country = await Injector.get(CountryService)?.getCountryByName(name)
    logger.info(`Get country ${JSON.stringify(country)}`)
    if(country){
      return country
    } 
    else{
      throw new Error('Country not exist')
    }
  } catch (error) {
    logger.error(`error has occured ${error}`)
    throw new createError.NotFound(error.message)
  }  
 
})

