import 'source-map-support/register'

import {Logger} from "@sailplane/logger";
import {Injector} from '@sailplane/injector'

import {CountryService} from '../services/country.service'

import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda'
import * as middy from 'middy'
import { cors } from 'middy/middlewares'


import * as createError from "http-errors";

let logger = new Logger('Save Countries');

export const handler = middy(async (event) => {
    try{
      const [record] = event.Records
      logger.info(`records Country ${record}`)
      const payload = Buffer.from(record.kinesis.data, 'base64').toString();
      logger.info(`records Country ${payload}`)
      const country = JSON.parse(payload)  
      logger.info(`Create Country ${country.name} with data ${JSON.stringify(country)}`)
    
      await Injector.get(CountryService)?.createCountry(country)
    
    }catch(error){
      logger.error(`error has occured ${error}`)
      throw new createError.InternalServerError(error.message)
    }
    
  })  
  
  handler.use(
    cors({
      credentials: true
    })
  )
  