import 'source-map-support/register'

import * as LambdaUtils from "@sailplane/lambda-utils";
import * as createError from "http-errors";

import { LoginRequest } from '../requests/loginUser'
import {Logger} from "@sailplane/logger";
import { Injector } from '@sailplane/injector';
import { CognitoService } from '../services/cognito.service';

let logger = new Logger('login cognito user');
export const handler = LambdaUtils.wrapApiHandler(async (event: LambdaUtils.APIGatewayProxyEvent) => { 
try{
    const user: LoginRequest =event.body!
    logger.info(`Create User ${user.email}`)
    let responce = await Injector.get(CognitoService)?.cognitoLogin(user)
    return responce
}catch(error){
    logger.error(`error has occured ${error}`)
    throw new createError.BadRequest(error.message)
}
})